package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
@RestController
//  will require all routes within the class to use the set endpoint
@RequestMapping("/greeting")
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}
	// localhost:8080/hello
	@GetMapping("/hello")
	// Maps a get request to the route "/hello" and invokes the method hello()
	public String hello(){
		return "Hello World!";
	}
	// Route with String query
	// localhost:8080/hi?name=value
	@GetMapping("/hi")
	// @RequestParam annotation that allows us to extract data from query strings in the url
	public String hi(@RequestParam(value="name", defaultValue = "John") String name){
		return String.format("Hi %s", name);
	}

	// Multiple Parameters
	// localhost:8080/friend?name=value&friend=value
	@GetMapping("/friend")
	public String friend(@RequestParam(value="name", defaultValue="Joe") String name, @RequestParam(value="friend", defaultValue="Jane") String friend){
		return String.format("Hello %s! My name is %s.", friend, name);
	}

	//Route with path variables
	// Dynamic data is directly obtained from the url
	// localhost:8080/name
	@GetMapping("/hello/{name}")
	// @PathVariable annotation allows us to extract data directly from the URL
	public String greetFriend(@PathVariable("name") String name){
		return String.format("Nice to meet you %s!", name);
	}

	// S09 - Intro to Spring Boot (Asynchronous Activity)
	// 1. Create a /welcome route that will accept multiple query string parameter (user & role) from a user and will
	// display the following:

	// localhost:8080/greeting/welcome/?user=value&role=value
	@GetMapping("/welcome")
	public String welcome(@RequestParam(value="user", defaultValue="Tolits") String user, @RequestParam(value="role", defaultValue="person") String role){
		String message;
		if (role.equals("admin")){
			message = String.format("Welcome back to the class portal, Admin %s", user);
		} else if (role.equals("teacher")) {
			message = String.format("Thank you for logging in, Teacher %s", user);
		} else if (role.equals("student")) {
			message = String.format("Welcome to the class portal, %s", user);
		} else {
			message = String.format("Role out of range");
		}
		return message;
	}

	// 2. Create a /register route that will accept multiple query string parameter (id, name, & course) from a user
	// and will perform the following:

	// localhost:8080/greeting/register/?id=value&name=value&course=value

	private ArrayList<Student> students = new ArrayList<>();
	@GetMapping("/register")
	public String register(@RequestParam(value="id", defaultValue="000000") String id, @RequestParam(value="name", defaultValue="John") String name, @RequestParam(value="course", defaultValue="none") String course){
		Student student = new Student(id, name, course);
		students.add(student);
		return String.format("%s your id number is registered on the system!", id);
	}

	// 3. Create a /account/id route that will use Path Variable and will perform the following:

	// localhost:8080/greeting/account/id
	@GetMapping("/account/{id}")
	public String account(@PathVariable("id") String id) {
		for(Student student : students){
			if (student.getId().equals(id)){
				return String.format("Welcome back %s! You are enrolled in %s", student.getName(), student.getCourse());
			}
		}
		return String.format("Your provided %s is not found in the system!", id);
	}

    // Activity
    // 1. Initialize a new ArrayList called enrollees in the DiscussionApplication class
    private ArrayList<String> enrollees = new ArrayList<>();

    // 2. Create a /enroll route that will accept a query string with the parameter of user which:
    // a. Adds the user's name in the enrollees array list
    // b. Returns a welcome (user)! Message.
    @GetMapping("/enroll")
    // localhost:8080/greeting/enroll/?user=value
    public String enroll(@RequestParam(value="user", defaultValue="Jerome") String user){
        enrollees.add(user);
        return String.format("Thank you for enrolling, %s", user);
    }

    // 3. Create a new /getEnrollees route which will return the content of the enrollees ArrayList as a String.
    @GetMapping("/getEnrollees")
    // localhost:8080/greeting/getEnrollees
    public String getEnroll(){
        return String.format(String.valueOf(enrollees));
    }

    // 4. Create a /nameage route with that will accept multiple query string parameters of name and age and will
    // return a Hello (name)! My age is (age). Message
    @GetMapping("/nameage")
    // localhost:8080/greeting/nameage/?name=value&age=value
    public String nameAge(@RequestParam(value="name", defaultValue="John") String name, @RequestParam(value="age", defaultValue="0") String age){
        return  String.format("Hello %s! My age is %s", name, age);
    }

    // 5. Create a /course/id dynamic route using a path variable of id
    @GetMapping("/courses/{id}")
    // http://localhost:8080/greeting/course/{value}
    public String courseId(@PathVariable("id") String id){
        if(id.equals("java101")){
            return String.format("Name: Java 101, Schedule: MWF 8:00AM-11:00AM, Price: PHP 3000.00");
        } else if (id.equals("sql101")) {
            return String.format("Name: SQL 101, Schedule: TTH 1:00PM-4:00PM, Price: PHP 2000.00");
        } else if (id.equals("javaee101")) {
            return String.format("Name: Java EE 101, Schedule: MWF 1:00PM-4:00PM, Price: PHP 3500.00");
        } else {
            return String.format("Course cannot be found!");
        }
    }


}
